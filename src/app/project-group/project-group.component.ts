import {Component, Input, OnInit} from "@angular/core";
import {Project} from "../project/Project";

@Component({
  selector: "app-project-group",
  templateUrl: "./project-group.component.html",
  styleUrls: ["./project-group.component.css"]
})
export class ProjectGroupComponent implements OnInit {
  @Input() name: string;
  @Input() projects: Project[];

  constructor() {
  }

  ngOnInit(): void {
  }

}
