import {Project} from "../project/Project";

export class ProjectGroup {
  public name: string;
  public projects: Project[];
}
