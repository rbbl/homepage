import {WebLink} from "./WebLink";

export class Project {
  public name: string;
  public status: string;
  public description: string;
  public links: WebLink[];
  public technologies: string[];
}
