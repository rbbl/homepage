export class WebLink {
  public name: string;
  public description: string;
  public url: string;
}
