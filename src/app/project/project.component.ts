import {Component, Input, OnInit} from "@angular/core";
import {WebLink} from "./WebLink";

@Component({
  selector: "app-project",
  templateUrl: "./project.component.html",
  styleUrls: ["./project.component.css"]
})
export class ProjectComponent implements OnInit {

  @Input() name: string;
  @Input() status: string;
  @Input() description: string;
  @Input() links: WebLink[];
  @Input() technologies: string[];
  @Input() groupMember = false;

  constructor() {
  }

  ngOnInit(): void {
  }

}
