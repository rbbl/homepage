import {Component, OnInit} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Profile} from "./models/Profile";

@Component({
  selector: "app-about",
  templateUrl: "./about.component.html",
  styleUrls: ["./about.component.css"]
})
export class AboutComponent implements OnInit {

  private profileUrl = "assets/profile.json";
  public profile: Profile;
  public fetchError: any;

  constructor(private httpClient: HttpClient) {
    const self = this;
    httpClient.get(this.profileUrl).subscribe({
      next(value): void {
        self.profile = value as Profile;
      }, error(error): void {
        self.fetchError = error;
      }
    });
  }

  ngOnInit(): void {
  }

}
