export class Profile {
  public basicInfo: BasicInfo;
  public companyRequirements: CompanyRequirements;
}

export class BasicInfo {
  public description: string;
  public technicalLanguages: TechnicalLanguage[];
  public tools: Tool[];
  public naturalLanguages: Language[];
}

export class TechnicalLanguage {
  public name: string;
  public skillInfo: string;
  public tools: string[];
}

export class Tool {
  public category: string;
  public items: string[];
}

export class Language {
  public name: string;
  public level: string;
}

export class CompanyRequirements {
  public description: string;
  public requirements: Requirement[];
}

export class Requirement {
  public type: string;
  public content: string;
}
