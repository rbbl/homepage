import {Component, OnInit} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Project} from "../project/Project";
import {ProjectGroup} from "../project-group/ProjectGroup";

@Component({
  selector: "app-projects",
  templateUrl: "./projects.component.html",
  styleUrls: ["./projects.component.css"]
})
export class ProjectsComponent implements OnInit {

  private projectsUrl = "assets/projects.json";
  public projects: (Project | ProjectGroup)[];

  constructor(private httpClient: HttpClient) {
    httpClient.get(this.projectsUrl).subscribe(value => {
      this.projects = value as (Project | ProjectGroup)[];
    });
  }

  ngOnInit(): void {
  }
}
