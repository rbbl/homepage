import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import {HttpClientModule} from "@angular/common/http";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { ProjectsComponent } from "./projects/projects.component";
import { AboutComponent } from "./about/about.component";
import { PageNotFoundComponent } from "./page-not-found/page-not-found.component";
import { ProjectComponent } from "./project/project.component";
import { ProjectGroupComponent } from "./project-group/project-group.component";
import {FormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    AppComponent,
    ProjectsComponent,
    AboutComponent,
    PageNotFoundComponent,
    ProjectComponent,
    ProjectGroupComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
